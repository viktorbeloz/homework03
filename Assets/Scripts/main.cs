﻿
using Assets.Scripts.com;
using com;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class main : MonoBehaviour
{
    MyList<string> mList = new MyList<string>();

    private void Reset()
    {
        mList.Add("Reset");
    }
    private void Awake()
    {
        mList.Add("Awake");
    }
    void Start()
    {
        mList.Add("Start");
    }
    private void OnEnable()
    {
        mList.Add("OnEnable");
    }
    private void FixedUpdate()
    {
        if (!mList.Contains("FixedUpdate"))
        {
            mList.Add("FixedUpdate");
        }
    }
    void Update()
    {
        if (!mList.Contains("Update"))
        {
            mList.Add("Update");
        }
    }
    private void LateUpdate()
    {
        if(!mList.Contains("LateUpdate"))
        {
            mList.Add("LateUpdate");
            
        }   
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        mList.Add("OnTrigerXXX");
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        mList.Add("OnCollisionXXX");
    }
    private void OnApplicationPause(bool pause)
    {
        mList.Add("OnApplicationPause");
    }
    private void OnDisable()
    {
        mList.Add("OnDisable");
    }
    private void OnApplicationQuit()
    {
        mList.Add("OnApplicationQuit");  
    }
    private void OnDestroy()
    {
        mList.Add("OnDestroy");
        EventManager.DispatchEvent(EventManager.GET_RESULT, new EventDataList(mList));
    }

}
