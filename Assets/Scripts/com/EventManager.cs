﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.com
{
    public class EventManager
    {
        static public string GO_CLICK = "goClick";
        static public string GET_RESULT = "getResult";

        static private Dictionary<string, List<Action<IData>>> _pool = new Dictionary<string, List<Action<IData>>>();

        static public bool AddListener(string eventName, Action<IData> callback)
        {

            if (_pool.ContainsKey(eventName))
            {
                if (_pool[eventName].Contains(callback))
                {
                    return false;
                }
                _pool[eventName].Add(new Action<IData>(callback));
                return true;
            }
            List<Action<IData>> l = new List<Action<IData>>();
            l.Add(callback);
            _pool.Add(eventName, l);
            return true;
        }

        static public bool DispatchEvent(string eventName, IData data)
        {
            if (_pool.ContainsKey(eventName))
            {
                foreach (var item in _pool[eventName])
                {
                    item(data);
                }
                return true;
            }

            return false;
        }

        static public bool RemoveEvent(string eventName, Action<IData> callback)
        {
            if (!_pool.ContainsKey(eventName))
            {
                return false;
            }
            if (_pool[eventName].Contains(callback))
            {
                _pool[eventName].Remove(callback);
                return true;
            }
            return false;
        }

        static public bool HasListener(string eventName, Action<IData> callback)
        {
            if (!_pool.ContainsKey(eventName))
            {
                return false;
            }
            if (!_pool[eventName].Contains(callback))
            {
                return false;
            }

            return true;
        }

    }
}
