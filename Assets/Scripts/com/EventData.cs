﻿using com;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.com
{
    public interface IData
    {

    }
    public class EventData : IData
    {
        public object data { get; set; }

        public EventData(object _data)
        {
            data = _data;
        }

        public override string ToString()
        {
            return $"{data}";
        }
    }
    public class EventDataList : IData
    {
        public MyList<string> dataL { get; set; }

        public EventDataList(MyList<string> _dataL)
        {
            dataL = _dataL;
        }

    }
}
