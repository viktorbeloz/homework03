﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com
{
    public class Node<T>
    {
        public T Data { get; set; }
        public Node<T> Next { get; set; }

        public Node(T data)
        {
            Data = data;
        }

    }
    public class MyList<T> : IEnumerable<T>
    {
        Node<T> head;
        Node<T> tail;
        public int Count;

        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);
            if (head is null)
            {
                head = node;
            }
            else
            {
                tail.Next = node;
            }
            tail = node;

            Count++;
        }

        public bool Remove(T data)
        {
            Node<T> previous = null;
            Node<T> current = head;
            while (current != null)
            {
                if (current.Data.Equals(data))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;

                        if (current.Next is null)
                        {
                            tail = previous;
                        }

                    }
                    else
                    {
                        head = current.Next;

                        if (head is null)
                        {
                            tail = null;
                        }

                    }
                    Count--;
                    return true;

                }
                previous = current;
                current = current.Next;
            }

            return false;
        }

        public bool Contains(T data)
        {
            Node<T> current = head;

            while (current != null)
            {
                if (current.Data.Equals(data))
                {
                    return true;
                }
                current = current.Next;
            }
            return false;
        }
        public T this[int index]
        {
            get
            {
                Node<T> current = head;
                if (index < Count && index >= 0)
                {
                    for (int i = 0; i < index; i++)
                    {
                        current = current.Next;
                    }
                    return current.Data;
                }
                else
                {
                    throw new NullReferenceException();
                }
            }
            set
            {
                Node<T> current = head;
                if (index < Count && index >= 0)
                {
                    for (int i = 0; i < index; i++)
                    {
                        current = current.Next;
                    }
                    current.Data = value;
                }
                else
                {
                    throw new NullReferenceException();
                }
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }
    }
}
