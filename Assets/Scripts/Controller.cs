﻿using Assets.Scripts.com;
using com;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    [SerializeField]
    private GameObject _obj;

    private void Awake()
    {       
        EventManager.AddListener(EventManager.GET_RESULT,GetResultEM);
    }
    private void Start()
    {
        _obj.SetActive(true);

    }
    private void GetResultEM(IData data)
    {
        var a = (EventDataList)data;
        MyList<string> l = a.dataL;

        foreach (var item in l)
        {
            Debug.Log(item);
        }
    }



}
